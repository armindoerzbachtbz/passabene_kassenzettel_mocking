package com.doerzbach;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class KassenzettelTest {

    private Kassenzettel k;
    @Mock
    private SuperCard superCard;
    @Mock
    private Produkt produkt1, produkt2, produkt3;

    @Mock
    private static RestApi restapi;

    @BeforeEach
    void setUp() {
        k = Kassenzettel.createKassenzettel(superCard);

    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void createKassenzettel() {
        assertEquals(superCard, k.getSuperCard(), "Superkarte nicht vorhanden");
    }

    @Test
    void addProdukt() {
        k.addProdukt(produkt1);
        assertEquals(1, k.getProdukte().size(), "Produkt nicht in liste");
        k.addProdukt(produkt2);
        assertEquals(2, k.getProdukte().size(), "2. Produkt nicht in liste");
        k.addProdukt(produkt1);
        assertEquals(2, k.getProdukte().size(), "Es sind nicht verschiedene 2 Produkte vorhanden");
        assertEquals(2, k.getProdukte().get(produkt1), "der Zaehler von Produkt 1 wurde nicht erhoeht");
    }

    @Test
    void removeProdukt() {
        k.addProdukt(produkt1);
        k.addProdukt(produkt1);
        k.removeProdukt(produkt1);
        assertEquals(1, k.getProdukte().size(), "Es wurden das ganze Produkt entfernt anstatt nur ein Stueck davon");
        k.removeProdukt(produkt1);
        assertEquals(0, k.getProdukte().size(), "Es wurde keine Leere Liste zurueckgeliefert sondern eine Liste mit einem Eintrag für Produkt1");

    }

    @Test
    void getProdukte() {
        assertNotNull(k.getProdukte(), "Es sollte immer eine Hashmap zurueckgeliefert werden auch wenn keine Produkte drin sind");
        assertEquals(1, k.getProdukte().size(), "Es wurde keine leere Liste zurueckgeliefert obwohl es so sein sollte");
        k.addProdukt(produkt1);
        assertEquals(1, k.getProdukte().get(produkt1));

    }

    @Test
    void transferToKasse() {
        MockedStatic<RestApi> restapiMock = mockStatic(RestApi.class);
        restapiMock.when(RestApi::getRestApi).thenReturn(restapi);
        String supercardid = "K00001";
        String product1id = "00001";
        when(produkt1.getID()).thenReturn(product1id);
        when(superCard.getID()).thenReturn(supercardid);
        String url = "/kunde/" + supercardid + "/kassenzettel";
        when(restapi.sendRequest(url, Method.POST, "")).thenReturn("0123");
        k.addProdukt(produkt1);
        k.transferToKasse();
        verify(restapi, times(1)).sendRequest(url, Method.POST, "");
        verify(restapi, times(1)).sendRequest(url + "/0123", Method.PUT, "{id:\"00001\",count:1}");

    }
}