package com.doerzbach;

import java.util.HashMap;

import static com.doerzbach.Method.POST;
import static com.doerzbach.Method.PUT;

public class Kassenzettel {

    private final SuperCard superCard;
    private final HashMap<Produkt, Integer> einkaufsliste = new HashMap<>();

    private Kassenzettel(SuperCard superCard) {
        this.superCard = superCard;
    }

    /**
     * Erzeugt einen Objekt der Klasse Kassenzettel
     */
    public static Kassenzettel createKassenzettel(SuperCard superCard) {
        return new Kassenzettel(superCard);
    }

    /**
     * Fügt ein Stück des Produktes p zu diesem Kassenzettel hinzu
     *
     * @param p: ein Produkt
     */
    public void addProdukt(Produkt p) {
        int i;
        if (einkaufsliste.containsKey(p)) {
            i = einkaufsliste.get(p);
            i++;
        } else {
            i = 1;
        }
        einkaufsliste.put(p, i);
    }

    /**
     * Nimmt ein Stück des Produktes p von diesem Kassenzettel weg
     *
     * @param p
     */
    public void removeProdukt(Produkt p) {

    }

    /**
     * Gibt eine HashMap der Produkte in diesem Kassenzettel zurück
     *
     * @return Enthält Produkte und deren Anzahl als Key-Value in HashMap.
     */
    public HashMap<Produkt, Integer> getProdukte() {
        return new HashMap<>();
    }

    /**
     * Transferiert die Produkte im Kassenzettel an die Kasse und benutzt dabei das RestApi
     */
    public void transferToKasse() {
        RestApi restapi = RestApi.getRestApi();
        String url="/kunde/"+superCard.getID()+"/kassenzettel";
        String id = restapi.sendRequest(url, POST, "");

        for (Produkt p : einkaufsliste.keySet()) {
            String data = "{id:\""+p.getID()+"\",count:"+einkaufsliste.get(p)+"}";
            String retval=restapi.sendRequest(url +"/"+ id, PUT,data);
        }

    }

    public SuperCard getSuperCard() {
        return superCard;
    }

}
