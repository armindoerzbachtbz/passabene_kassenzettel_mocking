# Mocking in Unit Tests

Stellen sie sich vor sie arbeiten bei Coop als Junior-Entwickler und sollen bei der Lancierung der neuen Selfscanning-App
mithelfen.

![Passabeneselfscanning-App](passabene.png)

Sie haben den Auftrag bekommen, genau eine Klasse der App zu entwickeln, nämlich die Klasse `Kassenzettel`.
![Alt text](Kassenzettel.drawio.png)
In dieser Klasse werden
- die Supercard registriert (`createKassenzettel`)
- die Produkte und Anzahl gespeichert (`addProdukt`,`removeProdukt` und `getProdukte`)
- die Produkte mit der Anzahl an die Kasse mit Hilfe der Klasse `RestApi` übertragen. (`transferToKasse`)

Die Interfaces für `RestApi`,`SuperCard` und `Product` wurden von ihren Kollegen zwar spezifieziert, aber eine implementation fehlt. 

Die Zeit drängt ,und sie sollten wenigstens ihre Klasse `Kassenzettel` programmieren und testen können. Was nun?

## Warum brauchen wir mocking?

*Mocking* braucht man, um unabhängig von assozierten Klassen, Unit-Tests durchführen zu können.
Man simuliert das erwartete Verhalten der assozierten Klassen ganz einfach mit einem *Mock*.

## Was bringt das in meiner Situation?

Ich kann einfach die Interfaces definieren, wie wir das abgemacht haben (siehe Klassendiagramm)
und diese *mocken*. 

So muss ich nicht warten, bis mein Kollege der die Klasse `Produkt` implementiert zurück aus den Ferien ist und der Langweiler der die Klassen `RestApi` und `SuperCard` programmieren sollte, endlich mal beginnt.

Was geschieht, wenn die beiden Kollegen die richtigen Klassen dann wirklich implementieren?

Dann werden sie einfach meine Interfaces, durch richtige Klassen ersetzen und es sollte alles wie gehabt funktionieren.

Zumindest habe ich dann schon einen grossen Teil meiner Klasse implementieren und testen können.

## Wie gehe ich vor?

1. Ich erstelle für alle Klassen die ich nicht selber programmiere und die noch nicht existieren ein Interface:
      - [Produkt](./src/com/doerzbach/Produkt.java)
      - [RestApi](./src/com/doerzbach/RestApi.java)
      - [SuperCard](./src/com/doerzbach/SuperCard.java)

   Für diese Interfaces und  definiere ich einfach alle Methoden die ich brauchen werde.

2. Das Enum `Method` erstelle ich soweit, wie ich es brauche zur Parameter-übergabe:
    - [Method](./src/com/doerzbach/Method.java)

3. Ich füge unter *Project-Structure->Modules->Dependencies* die Junit-Library (sucht nach `org.junit.jupiter` und wählt dann die neueste `org.junit.jupite:junit-jupiter-engine:....`-Library aus) und Mockito-Extensions (sucht nach `org.mockito` und wählt dann die neueste `org.mockito:mockito-junit-jupiter:...` aus) hinzu:

    ![Intellij Mockito](intellij_mockito.png)

4. Jetzt füge ich die *Mocks*, welche ich brauche, in der Testklasse [KassenzettelTest](./test/com/doerzbach/KassenzettelTest.java) hinzu:
  
    ```dtd
    @ExtendWith(MockitoExtension.class)
    public class KassenzettelTest {

        @Mock
        SuperCard superCard;
        @Mock
        Produkt produkt1,produkt2,produkt3;
        @Mock
        public static RestApi restapi;
    ```
    Dazu benutze ich `@Mock` für alle Variablen, die ich gemockt haben möchte hinzu und `@ExtendWith(MockitoExtension.class)`
    vor allen Testklassen in den ich *Mocking* verwenden möchte.

5. Die *gemockten* Objekte kann ich in der Testklasse ganz normal benutzen. Zum Beispiel in:
  
   ```dtd
   @BeforeEach
   void setUp() {
        k=Kassenzettel.createKassenzettel(superCard);

   }
   ```
   Hier wird das gemockte Objekt `superCard` von der Klasse `SuperCard` benutzt.
   oder
    ```dtd
    @Test
    void addProdukt() {
        k.addProdukt(produkt1);
        assertEquals(1,k.getProdukte().size(), "Produkt nicht in liste");
        k.addProdukt(produkt2);
        assertEquals(2,k.getProdukte().size(), "2. Produkt nicht in liste");
        k.addProdukt(produkt1);
        assertEquals(2,k.getProdukte().size(), "Es sind nicht verschiedene 2 Produkte vorhanden");
        assertEquals(2,k.getProdukte().get(produkt1), "der Zaehler von Produkt 1 wurde nicht erhoeht");
    }
    ```
    Hier werden die gemockten Objekte `produkt1`,`prdukt2` der Klasse `Produkt` benutzt.
   
5. Zusätzlich kann ich für jeden Aufruf einer Methode, wenn nötig einen Rückgabewert definieren:
    ```dtd
        String supercardid="K00001";
        String product1id="00001";
        when(produkt1.getID()).thenReturn(product1id); 
        when(superCard.getID()).thenReturn(supercardid);
        String url="/kunde/"+supercardid+"/kassenzettel";
        when(restapi.sendRequest(url, Method.POST, "")).thenReturn("0123");
    ```
    |                                                                                                                                                                     |
    |---------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | Wenn von der Instanz `produkt1` die Methode `getID()` aufgerufen wird, ist der Returnwert `"00001"`.                                                                |
    | Wenn von der Instanz `superCard` die Methode `getID()` aufgerufen wird ist der Returnwert `"K00001"`.                                                               |
    | Wenn von der Instanz `restapi` die Methode `sendRequest` mit genau den Parametern `(url, Method.POST, "")` aufgerufen, ist der Rückgabewert `"0123"`. Sonst `null`. |
    
    Bei allen Methoden der Rückgabewerte nicht mit `when().thenReturn()` definiert wurden, wird der default Wert zurückgegeben.
    
    Der default Rückgabewert ist, falls nicht anders gesetzt:
  
    | Für Typ                 	 | Returnwert 	|
    |---------------------------|------------	|
    | int,float,double 	        | 0          	|
    | boolean          	        | false      	|
    | Object           	        | null       	|

6. Singleton-Pattern für `RestApi` mocken:

    Das Mocken von Singletons ist nicht so einfach, da man `static`-Methoden mocken muss. 
    Um in unserem Beispiel die Klasse `RestApi` zu mocken, braucht es folgende Zeilen in der Testklasse:
    ```dtd
        MockedStatic<RestApi> restapiMock = mockStatic(RestApi.class);
        restapiMock.when(RestApi::getRestApi).thenReturn(restapi);
    ```
    und folgende Zeilen im Interface [RestApi](./src/com/doerzbach/RestApi.java):
    ```dtd
        public static RestApi getRestApi() {
            return null;
        }
    ```
    Erklärung dazu ist folgende. In meiner Klasse `Kassenzettel` in der Methode `transferToKasse` brauche ich
    die `RestApi` Klasse um Requests zu machen. Diese hat eine Methode `getRestApi` welche mir das Singleton
    dieser Klasse zurückgibt. Mit der Zeile

    ```dtd
       restapiMock.when(RestApi::getRestApi).thenReturn(restapi);
    ```
   
    wird genau diese `static`-Methode gemockt, und es wird das gemockte `restapi` Objekt aus der Klasse `KassenzettelTest`
    zurückgegeben.

7. Ich kann auch überprüfen wie oft eine Methode ausgeführt wurde:
   ```dtd
    verify(restapi,times(1)).sendRequest(url, Method.POST, "");
    verify(restapi,times(1)).sendRequest(url+"/0123", Method.PUT, "{id:\"00001\",count:1}");
   ```
   Mit diesen 2 Zeilen prüfe ich, dass `sendRequest` genau einmal mit diesen Paremetern aufgerufen wurde.

